# Qwerty? My Cat!

## Once upon a time...

Few months ago, we bought a connected cat door, installed on our garage door, for our cat named **Qwerty**.

A problem raised quickly, mainly from our kids: "_I cannot find the cat in the home. Is he hidding or is he out? Dad, look on your phone, please!_".

However, I do not live 24/7/365 with my phone on me (almost I have to admit), and then I got tired of that question.

This is why I investigated on the way to have a display where we could see where is the cat!

> This repository and code is the result of this!

## The backbone: the API

The first was to *reverse engineer* the API of SurePet, at least partially. I quickly identified several endpoints and some of their parameters (how could I be exhaustive?).

Don't worry, it is in my plan to share the result of these investigations! ;-)

As per this current project, it is not necessary to go more in detailts. I just want to mention I use 2 endpoints for this project (+ some other just for the helper...): authentication and pet/position!

# How to do your own display

If I share th code now, it is to also let you have your own diaplsy! And believe me, it won't take you hours to have it up and running!

## Hardware requirements

What I used (exhaustive list) is:

* Raspberry Pi Zero W/H - [Here on Pimoroni (sold out)](https://shop.pimoroni.com/products/raspberry-pi-zero-wh-with-pre-soldered-header)
* Pimoroni Inky pHat Black - [Here on Pimoroni](https://shop.pimoroni.com/products/inky-phat?variant=12549254938707)
* MicroSD card (8 Gb)
* Micro-usb powersupply with switch (like [this one](https://www.amazon.com/dp/B078YFMHR5) but with a EU plug)

***... and that's all!***

**Note**: I mentionned two shops above. I DO NOT have any interest in these companies and they (unfortunately) did not pay me in any manner to mention them. It is just to let you know what kind of hardware I used...

### RPi setup

Configure your RPi as followed:
* Connect to Wifi
* (Recommended) Setup to boot on console
* Enable the SPI interface option
* Enable the I2S interface option

You are a big boy or a big girl, you will find out how to setup this up... (Psst... Clue: ```raspi-config```!)

## Scripts setup

### First: get the fu...ng ```device_id```

There still is ONE parameter I cannot help you to get thanks to easy script: the ```device_id```. This ID (also called ```clientID```or ```uuid``` in the SurePet code) seems to be *calculated* by the website (and I guess the mobile application) and represent the device used to connect to the API. 

For instance, when I look at the API called *about me* (```/me/client```), here is what can find about ONE device (my webbrowser):
```javascript
{
    "id": 123456,
    "user_id": 12345,
    "platform": {
        "app": {
            "bundle_identifier": "browser",
            "version": "browser"
        },
        "device": {
            "model": {
                "manufacturer": "Google Inc.",
                "name": "Netscape"
            },
            "os": {
                "platform": "MacIntel",
                "version": "0.0.0"
            },
            "uuid": "1234567890"
        }
    },
    "version": "MA==",
    "created_at": "2020-03-20T20:15:17+00:00",
    "updated_at": "2020-03-20T20:15:17+00:00"
}
```

**Note**: I obviously offuscated the real IDs.

The required parameter is the one name ```platform.device.uuid```!

**TL;DR**

To get the ```device_id``` parameter:
* Open Chrome/Brave web browser (not tested on other browsers)
* Open the website [https://surepetcare.io/](https://surepetcare.io/)
* Open de DevTools by hitting F12
* Select the tab **console** if not selected by default
* At the bottom, next to the ```>```symbol, enter this harmless command: ```console.log(window.localStorage.deviceUuid)```
* Hit Enter and KEEP THE RESULT (a 10 digits number)
* To register this UUID at least once, *DO SIGN IN*

![Image](doc-images/uuid.png)





### Second: install the code on your RPi

Once you startup your RPi (use the GUI to connect to wifi, but then set it up to boot on the console), you need to install script sources

* Open a Terminal (if booted on GUI)
* Enter ```cd``` just to be sure to be in your home
* Enter ```git clone https://gitlab.com/arnaduga/qwerty-my-cat.git```

Good, now, everything is on your RPi!

***Important note***: I strongly recommend you to fork the project. You will be able then to create your OWN base image and update it easily! However, if having the Qwerty name displayed does not matter to you, fine! :)

### Launch the HELPER

Yes, I'm kind. To help you creating the *perfect* ```params.py``` file, I prepared something... Just launch the helper:
```bash
python3 helper.py
```

Let you guide and answer few questions:
```
 ╔══════════════════════════════════════════════════════╗
 ║             _    _      _                            ║░
 ║            | |  | |    | |                           ║░
 ║            | |__| | ___| |_ __   ___ _ __            ║░
 ║            |  __  |/ _ \ | '_ \ / _ \ '__|           ║░
 ║            | |  | |  __/ | |_) |  __/ |              ║░
 ║            |_|  |_|\___|_| .__/ \___|_|              ║░
 ║                          | |                         ║░
 ║                          |_|                         ║░
 ╚                                                      ╝░
  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░



This small "tool" helps you to retrieve your SurePet credentials.

Enter your SurePetCare username (surepetcare.io): myemail@company.com
Enter your password:
Enter your device_id (retrieve from browser JS console): 1234567890


> Getting your credentials...
> Retrieving your pet list...
>   Found 1 pet(s)
> Generating the config information...
> Please copy these lines and paste them into the params.py file:

user = "myemail@company.com"
password = "myPassword0987654321"
device_id = "1234567890"
pet_id = "34567"
isRPi = True


> This config file is READY for the Raspberry Pi
 _______ _           _   _             _ _    __      _ _        _
|__   __| |         | | ( )           | | |  / _|    | | |      | |
   | |  | |__   __ _| |_|/ ___    __ _| | | | |_ ___ | | | _____| |
   | |  | '_ \ / _` | __| / __|  / _` | | | |  _/ _ \| | |/ / __| |
   | |  | | | | (_| | |_  \__ \ | (_| | | | | || (_) | |   <\__ \_|
   |_|  |_| |_|\__,_|\__| |___/  \__,_|_|_| |_| \___/|_|_|\_\___(_)

```

If you have several pets, the helper will let choose the one you want to follow.


***Security note***: the password you will enter is NEVER send to internet, except to the SurePet API. everything is ran locally.

... and follow instructions!


## Schedule the refresh rate

When executed on the Raspberry Pi, the script will get information of your Pet's location, generate the image, display it and quit.

I wish I could even *shutdown* the RPi, but without extra component, I'm not able to swithc it on back some minutes later...

So, I used ```cron``` to schedule a regular update, every 5 minutes.

To do so, on the RPi console, edit the crontab:
```bash
sudo crontab -e
```


Add a line like this:
```
*/5 * * * * python3 /home/pi/qwerty-my-cat/main.py
```

Of course, may you want to change the frequency, please refer to the crontab documentation to understand all these ```*``` and their meaning ;-)


# Contribution

All improvements are welcome! Please get in touch if you can help in:
* Decompiling/understanding the Angular website to decrypt this UUID
* Want to work on reverse engineering the API
* IMprove/enhance the current display
* ... etc.


# Credits

Gitlab project icon from [FlatIcon](https://www.flaticon.com/), author [Smashicons](https://www.flaticon.com/authors/smashicons)>