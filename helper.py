# PLEASE NOTE
# As a quick and dirty helper, there is no exception management
#
# This is bad...
#
# ... I know that...
#
# ... However, this is not the core of the projet, so...
#
# ... ... ... ... ... ... Ok, one day I'll add some, you won...

print("")
print(
    " \u2554\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2557"
)
print(" \u2551             _    _      _                            \u2551\u2591")
print(" \u2551            | |  | |    | |                           \u2551\u2591")
print(" \u2551            | |__| | ___| |_ __   ___ _ __            \u2551\u2591")
print(" \u2551            |  __  |/ _ \\ | '_ \\ / _ \\ '__|           \u2551\u2591")
print(" \u2551            | |  | |  __/ | |_) |  __/ |              \u2551\u2591")
print(" \u2551            |_|  |_|\\___|_| .__/ \\___|_|              \u2551\u2591")
print(" \u2551                          | |                         \u2551\u2591")
print(" \u2551                          |_|                         \u2551\u2591")
print(" \u255A                                                      \u255D\u2591")
print(
    "  \u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591\u2591"
)


print('\n\n\nThis small "tool" helps you to retrieve your SurePet credentials.\n')


import getpass
import datetime
import requests
import sys


def log(txt):
    print("> %s" % (txt))


def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)


def getPetToken(user, passwd, devid):
    print("\n")
    log("Getting your credentials...")
    payload = {"email_address": user, "password": passwd, "device_id": devid}
    headers = {"Content-Type": "application/json"}
    response = requests.request(
        "POST",
        "https://app.api.surehub.io/api/auth/login",
        headers=headers,
        json=payload,
    )
    return response


def getPets(token):
    log("Retrieving your pet list...")
    tok = token.json()["data"]["token"]
    headers = {"Authorization": "Bearer " + tok}
    response = requests.request(
        "GET", "https://app.api.surehub.io/api/pet", headers=headers,
    )
    return response


def generateParams(username, password, device_id, pet_id, rpi):
    log("Generating the config information...")
    log("Please copy these lines and paste them into the params.py file:\n")
    print('user = "%s"' % (username))
    print('password = "%s"' % (password))
    print('device_id = "%s"' % (device_id))
    print('pet_id = "%s"' % (pet_id))
    print("isRPi = %s" % (rpi))
    print("\n")
    log("This config file is READY for the Raspberry Pi")


# Ask for credentials
username = input("Enter your SurePetCare username (surepetcare.io): ")
password = getpass.getpass(prompt="Enter your password:")
device_id = input("Enter your device_id (retrieve from browser JS console): ")

# Get the authentication token
authn = getPetToken(username, password, device_id)

# Call successful, but http/402
if authn.status_code == 401:
    log(
        "Something is not good. Check again username, password or device_id and try again!"
    )
    quit(1)

# Retrieve the PETS list
pets = getPets(authn)

# Select the appropriate PET (or ask to choose)
count = 0
for index, item in enumerate(pets.json()["data"]):
    count = count + 1

log("  Found %s pet(s)" % count)

if count == 0:
    print("No pet found. Exit.")
    quit(2)
if count == 1:
    thePet = pets.json()["data"][0]
if count > 1:
    print("Which pet do you want to follow?")
    for index, item in enumerate(pets.json()["data"]):
        print(
            "%s. %s (id: %s)"
            % (
                index + 1,
                pets.json()["data"][index]["name"],
                pets.json()["data"][index]["id"],
            )
        )
    selected = input("Select pet (1-%s): " % (count))
    thePet = pets.json()["data"][int(selected) - 1]

generateParams(username, password, device_id, thePet["id"], True)

print(" _______ _           _   _             _ _    __      _ _        _")
print("|__   __| |         | | ( )           | | |  / _|    | | |      | |")
print("   | |  | |__   __ _| |_|/ ___    __ _| | | | |_ ___ | | | _____| |")
print("   | |  | '_ \\ / _` | __| / __|  / _` | | | |  _/ _ \\| | |/ / __| |")
print("   | |  | | | | (_| | |_  \\__ \\ | (_| | | | | || (_) | |   <\\__ \\_|")
print("   |_|  |_| |_|\\__,_|\\__| |___/  \\__,_|_|_| |_| \\___/|_|_|\\_\\___(_)")
print("")
print("")
print("")
