# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2020.03.25 - v1.0.0]
### Added
- Project creation
- Helper ```helper.py``` to guide the user in creating the ```params.py``` file
- Created the ```todo.md```to follow ... todos (may be useless if I decide to use the Gitlab capabilities.... probably)

### Changed
- ```params.py``` principles to externalize parameters
- main asset ```whereisthecat.svg``` file for a better look (personal perception)
- dramatically improve doc (```readme.md```)

