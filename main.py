import time
import datetime
from dateutil import tz
import os
from PIL import Image, ImageDraw, ImageFont
from font_fredoka_one import FredokaOne
import requests
import json


def log(txt):
    print("%s - %s" % ("{:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()), txt))


log("Loading parameters...")

import params

baseURL = "https://app.api.surehub.io/api"


def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)


def getPetToken():
    log("[getPetToken] Defining parameters")
    payload = {
        "email_address": params.user,
        "password": params.password,
        "device_id": params.device_id,
    }
    headers = {"Content-Type": "application/json"}
    log("[getPetToken] Calling the API")
    response = requests.request(
        "POST", baseURL + "/auth/login", headers=headers, json=payload
    )
    log("[getPetToken] Formatting response")
    return response.json()["data"]["token"]


def getPetPosition():
    log("[getPetPosition] Defining parameters")
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer %s" % (getPetToken()),
    }
    log("[getPetPosition] Calling the API")
    response = requests.request(
        "GET", baseURL + "/pet/" + params.pet_id + "/position", headers=headers
    )

    # Format response
    log("[getPetPosition] Formatting response")
    to_zone = tz.tzlocal()
    utc = datetime.datetime.fromisoformat(response.json()["data"]["since"])
    local = utc.astimezone(to_zone)
    res = {
        "status": "ok",
        "position": "IN" if response.json()["data"]["where"] == 1 else "OUT",
        "since": local,
    }
    return res


def generateImage():
    log("[generateImage] Get pet position...")
    whereIsTheCat = getPetPosition()
    log("[generateImage] Get pet position... Done")

    log("[generateImage] Preparing response")
    if whereIsTheCat["position"] == "IN":
        srcFile = os.path.dirname(os.path.realpath(__file__)) + "/assets/in.png"
        xOffset = 40
        yOffset = 0
    else:
        xOffset = -60
        yOffset = 10
        srcFile = os.path.dirname(os.path.realpath(__file__)) + "/assets/out.png"

    log("[generateImage] Opening base image file")
    img = Image.open(srcFile)
    draw = ImageDraw.Draw(img)

    log("[generateImage] Add SINCE date")
    font = ImageFont.truetype(FredokaOne, 30)
    message = whereIsTheCat["since"].strftime("%H:%M")
    w, h = font.getsize(message)
    x = xOffset + (212 / 2) - (w / 2)
    y = yOffset + (107 / 2) - (h / 2)
    draw.text((x, y), message, fill="black", font=font)

    # For the label
    label = "Depuis"
    font = ImageFont.truetype(FredokaOne, 14)
    x = xOffset + (212 / 2) - (w / 2)
    y = yOffset - 10 + (107 / 2) - (h / 2)
    draw.text((x, y), label, fill="black", font=font)

    log("[generateImage] Add update date")
    font = ImageFont.truetype(FredokaOne, 10)
    messageTimestamp = "%s" % ("{:%H:%M}".format(datetime.datetime.now()))
    w, h = font.getsize(messageTimestamp)
    x = 2
    y = 104 - h
    draw.text((x, y), messageTimestamp, fill="black", font=font)
    del draw

    log("[generateImage] Save the IMAGE")
    img = img.quantize()
    if params.isRPi == False:
        img.save("tmp/output.png", "PNG")
    return img


im = generateImage()


# If on Raspberry PI, display this imager to the pHat
if params.isRPi:
    from inky import InkyPHAT

    inky_display = InkyPHAT("black")
    inky_display.set_border(inky_display.BLACK)

    # img = Image.open("output.png")
    inky_display.set_image(im)
    inky_display.show()
